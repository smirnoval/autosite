# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth.models import User
from django.db import models
import datetime

class Banner(models.Model):
    banner_text = models.TextField('banner_text')

    def __str__(self):
        return self.banner_text.encode('utf8')

class ListItems(models.Model):
    title = models.TextField('title')

    def __str__(self):
        return self.title.encode('utf8')

class Item(models.Model):
    item_text = models.TextField('item_text')
    list_items = models.ForeignKey(ListItems, on_delete=models.CASCADE)


class Article(models.Model):
    article_title = models.TextField('article_title')
    article_short_description = models.TextField('short_description')
    article_text = models.TextField('article_text')
    article_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.article_title.encode('utf8')

class Question(models.Model):
    author_id = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.TextField('article_title')
    description = models.TextField('description')
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.title.encode('utf8')

class Comment(models.Model):
    author_id = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField('description')
    pub_date = models.DateTimeField(auto_now_add=True, blank=True)
    question_key = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.description.encode('utf8')

class UserProfile(models.Model):
    user = models.OneToOneField(User)
    chat_access = models.BooleanField(default=False)

    def __unicode__(self):
        return unicode(self.user)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

class Image_Link(models.Model):
    name = models.CharField(max_length=255)
    link = models.TextField('link')
    article_id = models.ForeignKey(Article, on_delete=models.CASCADE)

    def __str__(self):
        return self.name.encode('utf8')