from django.conf.urls import url
from mainpage import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^questions/$', views.questions),
    url(r'^questions/(?P<pk>[0-9]+)/$', views.get_one_question, name='get_one_question'),
    url(r'^questions/(?P<pk>\d+)/comment/$', views.add_comment_to_question, name='add_comment_to_question'),
    url(r'^articles/$', views.articles),
    url(r'^articles/(?P<pk>[0-9]+)/', views.get_one_article, name='get_one_article'),
    url(r'^questions/new/$', views.post_new, name='post_new'),
    url(r'^questions/(?P<pk>\d+)/get_comments/$', views.get_comments_for_question, name='get_comments_for_question'),

    #Login, Logout, Register
    url(r'^login/$', views.ajax_login, name='ajax_login'),
    url(r'^logout/$', views.ajax_logout, name='ajax_logout'),
]