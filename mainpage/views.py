from django.shortcuts import render, get_object_or_404
from mainpage.models import Banner, Article, ListItems, Item, Question, Comment, Image_Link
from mainpage.forms import QuestionForm, CommentForm
from django.template import Context, loader
from django.http import HttpResponse
import json
from django.shortcuts import redirect
from datetime import timedelta
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout

our_time_zone= timedelta(days=0, hours=3, seconds=0)

def articles(request):
    articles = Article.objects.order_by('id')[::-1]
    template = loader.get_template('mainpage/articles.html')
    context_dic = {}
    context_dic['user'] = request.user
    context_dic['articles'] = articles
    context = Context({'context_dic': context_dic})
    return HttpResponse(template.render(context))

def get_one_article(request, pk):
    article = get_object_or_404(Article, pk=pk)
    images = Image_Link.objects.all().filter(article_id=pk)
    template = loader.get_template('mainpage/article.html')
    context_dic = {}
    context_dic['user'] = request.user
    context_dic['article'] = article
    context_dic['images'] = images
    context = Context({'context_dic': context_dic})
    return HttpResponse(template.render(context))

def questions(request):
    questions = Question.objects.order_by('id')[::-1]
    template = loader.get_template('mainpage/questions.html')
    context_dic = {}
    context_dic['user'] = request.user
    context_dic['questions'] = questions
    context = Context({'context_dic': context_dic})
    return HttpResponse(template.render(context))

def get_one_question(request, pk):
    question = get_object_or_404(Question, pk=pk)
    comments = Comment.objects.all().filter(question_key=pk)
    template = loader.get_template('mainpage/show_question.html')
    context_dic = {}
    context_dic['question'] = question
    context_dic['comments'] = comments
    context_dic['user'] = request.user
    context_dic['form'] = CommentForm()
    context = Context({'context_dic': context_dic})
    return HttpResponse(template.render(context))

def get_comments_for_question(request, pk):
    comments = Comment.objects.all().filter(question_key=pk)
    data = [{'id': str(item.author_id), 'description': item.description, 'pub_date': (item.pub_date+our_time_zone).strftime('%Y %m %d %H %M')} for item in comments]
    print(data)
    return HttpResponse(json.dumps(data), content_type='application/json')

def add_comment_to_question(request, pk):
    question = get_object_or_404(Question, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.question_key = question
            comment.author_id = request.user
            comment.save()
            return redirect('mainpage.views.get_one_question', pk=question.pk)
    else:
        form = CommentForm()
    return render(request, 'mainpage/add_comment.html', {'form': form})

def index(request):
    banner_text = Banner.objects.order_by('id')[::-1]
    banner_text = banner_text[0]
    item_lists = ListItems.objects.all()
    items = []
    for i in item_lists:
        items.append(Item.objects.all().filter(list_items=i.id))
    context_dic = {}
    context_dic['user'] = request.user
    context_dic['banner_text'] = banner_text
    context_dic['item_lists'] = item_lists
    context_dic['items'] = items
    template = loader.get_template('mainpage/index.html')
    context = Context({'context_dic': context_dic})
    return HttpResponse(template.render(context))

def post_new(request):
    if request.method == "POST":
        form = QuestionForm(request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.author_id = request.user
            question.save()
            return redirect('mainpage.views.get_one_question', pk=question.pk)
    else:
        form = QuestionForm()
    context_dic = {}
    context_dic['user'] = request.user
    context = Context({'context_dic': context_dic})
    return render(request, 'mainpage/question.html', {'form': form, 'context_dic': context_dic})

def ajax_login(request):
    username = request.POST['username']
    password = request.POST['password']
    print(username)
    print(password)
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponse("Success!")
        else:
            return HttpResponse("Fail!")
    else:
        return HttpResponse("You are invalid! This account not exist!")

def ajax_logout(request):
    logout(request)
    return HttpResponse("Success!")