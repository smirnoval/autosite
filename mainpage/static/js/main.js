//alert("Hi!");

 $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();
  });

$(document).ready(function() {
    // CSRF code
    function getCookie(name) {
        var cookieValue = null;
        var i = 0;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (i; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    setTimeout(addEventListeners, 500);
});

function addEventListeners() {
    var post_ajax = document.getElementById('post_ajax');
    if(post_ajax) {
        post_ajax.addEventListener("click", postAjax, false);
    }

    var ajax_login = document.getElementById('ajax_login');
    if(ajax_login) {
        ajax_login.addEventListener("click", ajaxLogin, false);
    }

    var ajax_logout = document.getElementById('ajax_logout');
    if(ajax_logout) {
        ajax_logout.addEventListener("click", ajaxLogout, false);
    }

    var ajax_login_change = document.getElementById('login_change_ajax');
    if(ajax_login_change) {
        ajax_login_change.addEventListener("click", ajaxLoginChange, false);
    }

    var post_message_ajax = document.getElementById('post_message_ajax');
    if(post_message_ajax) {
        post_message_ajax.addEventListener("click", ajaxPostMessage, false);
    }

    var show_fields_for_add_image = document.getElementById('show_fields_for_add_image');
    if(show_fields_for_add_image) {
        show_fields_for_add_image.addEventListener("click", ajaxShowAddImage, false);
    }
}

function postAjax() {
    $.post(location.href+"comment/", {
        description: $("#id_description").val()
    },
    function(data) {
        var post_description = document.getElementById('id_description');
        post_description.value='';
        $.get(location.href+"get_comments/", function(data) {
            var all_comments = document.getElementById('all_comments');
            if(all_comments) {
                all_comments.textContent = '';
                for(i=0; i<data.length; i++) {
                    var col = document.createElement('div');
                    col.className = 'col s6 offset-s3 no_ident';
                    all_comments.appendChild(col);

                    var card = document.createElement('div');
                    card.className = 'card blue';
                    col.appendChild(card);

                    var card_content = document.createElement('div');
                    card_content.className = 'card-content white-text';
                    card.appendChild(card_content);

                    var span = document.createElement('span');
                    span.className = 'card-title';
                    card_content.appendChild(span);

                    var pub_date = document.createElement('p');
                    pub_date.textContent = data[i].pub_date;
                    card_content.appendChild(pub_date);

                    var id = document.createElement('h6');
                    id.textContent = data[i].id;
                    card_content.appendChild(id);

                    var description = document.createElement('h6');
                    description.textContent = data[i].description;
                    card_content.appendChild(description);
                }
            }
        });
    });
}

function ajaxLogin() {
    $.post("/login/", {
        username: $("#id_username").val(), password: $("#id_password").val()
    },
    function(data) {
        alert(data)
        if (data == "Success!") {
            location.reload(true)
        }
        else {
            alert("Перезагрузка не удалась!")
        }

    });
}

function ajaxLogout() {
    $.post("/logout/", {
    },
    function(data) {
        alert(data)
        if (data == "Success!") {
            location.reload(true)
        }
        else {
            alert("Перезагрузка не удалась!")
        }
    });
}

function ajaxLoginChange() {
    $.post("/profile/", {
        username: $("#change_username").val()
    },
    function(data) {
        if (data == "Success!") {
            location.reload(true)
        }
        else {
            alert("Перезагрузка не удалась!")
        }
    });
}

function ajaxPostMessage() {
    images = $("#image").val();
    text = $("#text").val();
    images = '<img class="materialboxed" data-caption="' + text + '" width="250" src="' + images + '">';
    $.post("/forum/add/", {
        text: $("#message").val(), images: images
    },
    function(data) {
        alert(data);
        location.reload(true);
    });
}

function ajaxShowAddImage() {
    document.getElementById("show_add_image").style.display = "block"
    document.getElementById("show_fields_for_add_image").removeEventListener('click', ajaxShowAddImage, false);
    document.getElementById("show_fields_for_add_image").remove();
}