from django import forms
from mainpage.models import Question, Comment

class QuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ('title', 'description',)

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('description',)