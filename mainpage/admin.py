from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from mainpage.models import Banner, Article, ListItems, Item, Question, Comment, Image_Link
from mainpage.models import UserProfile
from django.contrib.auth.models import User


class ItemInline(admin.StackedInline):
    model = Item
    extra = 3

class ListItem(admin.ModelAdmin):
    fieldsets = [
        ('Title',               {'fields': ['title']}),
    ]
    inlines = [ItemInline]

class Commentnline(admin.StackedInline):
    model = Comment

class ListQuestion(admin.ModelAdmin):
    fieldsets = [
        ('Title',               {'fields': ['description']}),
    ]
    inlines = [Commentnline]

class UserInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'Extra information'

class UserAdmin(UserAdmin):
    inlines = (UserInline, )


class ImageServerInLine(admin.StackedInline):
    model = Image_Link
    extra = 3

class ImageServerLine(admin.ModelAdmin):
    inlines = [ImageServerInLine]

admin.site.register(ListItems, ListItem)
admin.site.register(Banner)
admin.site.register(Question, ListQuestion)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Article, ImageServerLine)