from django.conf.urls import url
from forum import views

urlpatterns = [
    url(r'^$', views.main),
    url(r'^add/$', views.add_comment),
]