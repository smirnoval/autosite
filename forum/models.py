from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Message(models.Model):
    author_id = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField('text')
    images = models.TextField('images')
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):
        return self.text.encode('utf8')