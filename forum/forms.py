from django import forms
from forum.models import Message

class CommentForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ('text', 'images')