from django.shortcuts import render
from django.template import Context, loader
from django.http import HttpResponse
from forum.models import Message
from forum.forms import CommentForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

def main(request):
    all_messages = Message.objects.order_by('id')[::-1]
    paginator = Paginator(all_messages, 20)
    page = request.GET.get('page')
    try:
        messages = paginator.page(page)
    except PageNotAnInteger:
        messages = paginator.page(1)
    except EmptyPage:
        messages = paginator.page(paginator.num_pages)
    template = loader.get_template('forum/main.html')
    context_dic = {}
    context_dic['user'] = request.user
    context_dic['messages'] = messages
    context = Context({'context_dic': context_dic})
    return HttpResponse(template.render(context))

def add_comment(request):
    if request.is_ajax():
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author_id = request.user
            print(comment)
            comment.save()
            return HttpResponse("Success!")
        else:
            return HttpResponse("Form is failed!")
    else:
        return HttpResponse("Fail link!")