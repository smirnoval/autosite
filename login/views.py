from login.forms import *
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.template import Context, loader
from django.http import HttpResponse

from django.shortcuts import render

@csrf_protect
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password1'],
            email=form.cleaned_data['email']
            )
            return HttpResponseRedirect('/')
    else:
        form = RegistrationForm()
    variables = RequestContext(request, {
    'form': form
    })

    return render_to_response(
    'login/register.html',
    variables,
    )

def profile(request):
    context_dic = {}
    context_dic['user'] = request.user
    if request.is_ajax():
        username = request.POST['username']
        print(username)
        user = request.user
        user.username = username
        user.save()
        return HttpResponse("Success!")
    else:
        context = Context({'context_dic': context_dic})
        return render(request, 'login/profile.html', context)